import sys
import os
import json

from urllib.parse import quote
import markdown


def group_entry_string(group):
  return f"    * [{group['name']}]({group['url']})\n"  

def list_by_alphapet(groups):
  markdown_string = ""
  letter = None
  groups.sort(key=lambda g: g['name'])
  for group in groups:
    first_letter = group['name'][0].upper()
    if first_letter != letter:
      letter = first_letter
      markdown_string += f"\n* **{letter}** \n" 
    markdown_string += group_entry_string(group)
  return markdown_string

def get_tags(groups):
  tags = []
  for tag_list in map(lambda g: g.get('tags',[]), groups):
    tags += tag_list
  return sorted(set(tags))  

def get_groups(groupfolder):
  groups = []
  for filename in os.listdir(groupfolder):
    if not filename.endswith(".json"):
      continue
    with open(os.path.join(groupfolder,filename),"rb") as fp:
      group = json.load(fp)
      group["filename"] = filename
      groups.append(group)  

  return sorted(groups, key=lambda g: g['name'])

def get_groups_by_tag(groups,tag):
  return list(filter(lambda g: tag in g.get('tags',[]),list(groups)))
  

def list_by_tags(groups):
  markdown_string = ""
  tags = get_tags(groups)
  for tag in tags:
    markdown_string += f"\n* **{tag}** \n"
    filterd_groups = get_groups_by_tag(groups, tag)
    for group in filterd_groups:
      markdown_string += group_entry_string(group)  
  return markdown_string


def build_html_page(html_filename,groupfolder="groups"):
  groups = []
  markdown_string = ""
  with open("intro.md") as intro_fp:
    markdown_string += intro_fp.read()

  groups = get_groups(groupfolder)
  
  markdown_string += "\n## Alphabetisch\n"
  markdown_string += list_by_alphapet(groups)    
  markdown_string += "\n## Nach Thema/Tags\n"
  markdown_string += list_by_tags(groups)   
  
  
  with open("template.html","r") as template_fp:
    html = template_fp.read().replace("{{{content}}}",markdown.markdown(markdown_string))
  with open(html_filename, "w", encoding="utf-8", errors="xmlcharrefreplace") as html_fp:
    html_fp.write(html)


def build_api(base_url=os.getenv("CI_PAGES_URL",None),groupfolder="groups", api_json_name = "api.json"):
  if base_url is None:
    raise Exception("no base url set")
  groups = get_groups(groupfolder)
  for group in groups:
    group["api_url"] = f"{base_url}/group/{quote(group['filename'])}"
  tag_strings = get_tags(groups)
  tags = []
  tag_folder = "tag"
  if not os.path.exists(tag_folder):
    os.mkdir(tag_folder)
  
  for tag in tag_strings:
    tag_data = {"name":tag, "api_url": f"{base_url}/tag/{quote(tag)}.json", "groups": get_groups_by_tag(groups, tag)}
    tags.append(tag_data)   
    with open(os.path.join(tag_folder,f"{tag}.json"),"w", encoding="utf-8") as fp:
      json.dump(tag_data, fp, ensure_ascii=False)
  for group in groups:
    del group["filename"]
  data= {
    "api_url": f"{base_url}/{api_json_name}",
    "groups": groups,
    "tags" : tags
    }
  with open(api_json_name, "w", encoding="utf-8") as fp:
    json.dump(data, fp, ensure_ascii=False)


if __name__ == "__main__":
  html_filename = "index.html"
  if len(sys.argv) == 2:
    html_filename = sys.argv[1]
  build_html_page(html_filename)
  build_api()